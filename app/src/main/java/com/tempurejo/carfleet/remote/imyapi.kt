package com.tempurejo.carfleet.remote

import com.tempurejo.carfleet.model.APIResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface imyapi{
    @FormUrlEncoded
    @POST("login")
    fun loginuser(@Field("username") username: String,@Field("password") password: String):Call<APIResponse>
}