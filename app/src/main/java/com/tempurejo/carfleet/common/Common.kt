package com.tempurejo.carfleet.Common

import com.tempurejo.carfleet.remote.RetrofitClient
import com.tempurejo.carfleet.remote.imyapi

object Common {
    val BASE_URL="http://36.92.115.146/flm/backend/public/api/admin/"

    val api:imyapi
    get() = RetrofitClient.getClient(BASE_URL).create(imyapi::class.java)
}