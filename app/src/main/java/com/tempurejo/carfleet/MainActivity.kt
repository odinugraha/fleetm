package com.tempurejo.carfleet

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.tempurejo.carfleet.Common.Common
import com.tempurejo.carfleet.model.APIResponse
import com.tempurejo.carfleet.remote.imyapi
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    internal lateinit var mService: imyapi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mService = Common.api
        txt_login.setOnClickListener {
            aunthenticateUser(
                username.text.toString(),
                password.text.toString()
            )
        }
    }

    private fun aunthenticateUser(username: String, password: String) {
        mService.loginuser(username, password)
            .enqueue(object : Callback<APIResponse> {
                override fun onFailure(call: Call<APIResponse>, t: Throwable) {
                    Toast.makeText(this@MainActivity, t!!.message, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {

                    if (response!!.body()!!.status?.error==0) {
                        Toast.makeText(
                            this@MainActivity,
                            "Login Success",
                            Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent(this@MainActivity, Bottomnav::class.java)
                        startActivity(intent)
                        finish()
                    }
                    else{
                        Toast.makeText(
                            this@MainActivity,
                            response!!.body()!!.status?.message,
                            Toast.LENGTH_SHORT
                       ).show()
                    }
                }

            })
    }
}
