package com.tempurejo.carfleet.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Status {

    @SerializedName("error")
    @Expose
    var error: Int? = null
    @SerializedName("errorCode")
    @Expose
    var errorCode: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

}