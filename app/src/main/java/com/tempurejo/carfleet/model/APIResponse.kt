package com.tempurejo.carfleet.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class APIResponse {

    @SerializedName("status")
    @Expose
    var status: Status? = null

}