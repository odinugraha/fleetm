package com.tempurejo.carfleet.model

data class Driver(val lat: Double, val lng: Double, val driverId: String = "0000")