package com.tempurejo.carfleet.interfaces

@FunctionalInterface
interface IPositiveNegativeListener {

    fun onPositive()

    fun onNegative() {

    }
}